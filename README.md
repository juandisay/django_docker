# Django Docker

## About Django Docker

This repository is about docker conf to serve django web with selenium web scraper.

## How to use 

### Export DigitalOcean API ###

1. Make sure you have DigitalOcean account, if you have no account yet, you can signup first.
2. Generate token API.
3. Export the key into your env `$ export DIGITAL_OCEAN_ACCESS_TOKEN=[your_token]`

### Deploy Selenium Grid to Digital Ocean ###

1. Run `sh selenium_grid/create.sh` (this script will create docker machine in Digital Ocean, and set the docker swarm manager and docker swarm workers)

2. Deploy to docker swarm service  
`eval $(docker-machine env node-1)`  
`docker stack deploy --compose-file=selenium-grid.yml selenium`  
`docker service scale selenium_chrome=5`  

3. Then run this command to get the selenium-hub IP  
`docker-machine ip $(docker service ps --format "{{.Node}}" selenium_hub)`  

4. Copy the IP and use it into *SELENIUM_HUB_IP* in `docker.env`


### Deploy Django Docker to Digital Ocean ###

1. Create docker machine  
`docker-machine create \
--driver digitalocean \
--digitalocean-access-token $DIGITAL_OCEAN_ACCESS_TOKEN \
--digitalocean-size s-1vcpu-2gb \
django-docker;`  
`eval $(docker-machine env django-docker)`

2. Copy code to docker host  
`rsync -av -e 'docker-machine ssh django-docker' nginx :`  
`rsync -av -e 'docker-machine ssh django-docker' django_selenium :`

3. Configure `docker.env` (set PROXY_IP to *django-docker* IP)

4. Up the docker container  
`docker-compose -f docker-compose-do.yml up -d mysql`  
`docker-compose -f docker-compose-do.yml run web python manage.py migrate`  
`docker-compose -f docker-compose-do.yml run web python manage.py createsuperuser`  
`docker-compose -f docker-compose-do.yml up -d`  

5. Check into your browser with docker machine ip
6. Enter into admin page in `/admin` and then input the username and password

### Setup Luminati Proxy ###

1. Register Luminati account
2. Create zone with number of IPs (more than 1, to rotate IPs)
3. Open Proxy Manager (docker machine ip in port 22999) and login with luminati account
4. Start the proxy in port 24000 with the zone that has been created before (default static), then preset configuration select to round robin, and set pool size to number IPs we have

For more detail, please look at this Wiki [Luminati Proxy](https://bitbucket.org/aijogja/django_docker/wiki/Luminati%20Proxy)

### To add key ###

1. Go to __Amazon_Product > Items__ menu, and add.

### To set crontab ###

1. Go to __Djcelery > Crontabs__ menu, and add the daily cron setting __(minute=0, hour=0)__, then save.
2. Go to __Djcelery > Periodic tasks__ menu, and add the periodic task __(name='Amazon product crawl', task custom='*amazon_product.tasks.update_all_items*', crontab choose the created before)__
