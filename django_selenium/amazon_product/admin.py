from django.contrib import admin
from amazon_product.models import Item, ItemProperty

# Register your models here.


class ItemAdmin(admin.ModelAdmin):
    fields = ('key', )
    list_display = (
        'key', 'name', 'get_price', 'get_num_customer_reviews',
        'get_rate_customer_reviews', 'get_num_questions_answers',
        'get_in_stock', 'get_last_crawl'
    )

    def get_price(self, obj):
        return obj.item_properties.data.get('price')
    get_price.short_description = 'Price'

    def get_num_customer_reviews(self, obj):
        return obj.item_properties.data.get('num_customer_reviews')
    get_num_customer_reviews.short_description = 'Num Customer Review'

    def get_rate_customer_reviews(self, obj):
        return obj.item_properties.data.get('rate_customer_reviews')
    get_rate_customer_reviews.short_description = 'Rate Customer Review'

    def get_num_questions_answers(self, obj):
        return obj.item_properties.data.get('num_customer_questions_answers')
    get_num_questions_answers.short_description = 'Num Questions/Answers'

    def get_in_stock(self, obj):
        return obj.item_properties.data.get('in_stock')
    get_in_stock.short_description = 'In Stock'

    def get_last_crawl(self, obj):
        return obj.item_properties.updated
    get_last_crawl.short_description = 'Last Crawl'

admin.site.register(Item, ItemAdmin)
