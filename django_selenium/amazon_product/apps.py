from django.apps import AppConfig


class AmazonProductConfig(AppConfig):
    name = 'amazon_product'
