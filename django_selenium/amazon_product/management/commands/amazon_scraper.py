from django.core.management.base import BaseCommand, CommandError
from amazon_product.models import Item
from amazon_product.utils import get_amazon_product_data


class Command(BaseCommand):
    help = 'Amazon product scraper'

    def handle(self, *args, **options):
        items = Item.objects.all()[:1]
        for item in items:
            data = get_amazon_product_data(item.key)
            name = data.pop('name', None)
            item.name = name
            item.save()
            item.item_properties.data = data
            item.item_properties.save()
