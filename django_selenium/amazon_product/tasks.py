from __future__ import absolute_import
from celery import shared_task, task
from django_selenium.celery import app
from amazon_product.models import Item
from amazon_product.utils import get_amazon_product_data


@app.task
def amazon_scrapper(pk):
    item = Item.objects.get(pk=pk)
    data = get_amazon_product_data(item.key)
    item.name = data.pop('name', None)
    item.save()
    item.item_properties.data = data
    item.item_properties.save()
    print('Item updated')


@app.task
def update_all_items():
    items = Item.objects.all()
    for item in items:
        amazon_scrapper.delay(item.pk)
